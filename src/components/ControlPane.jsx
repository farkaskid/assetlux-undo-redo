import React from 'react';
import ShapeSelect from './ShapeSelect';
import UndoRedoButtons from './UndoRedoButtons';
import UpdateButton from './UpdateButton';
import ColorInput from './ColorInput';

const ControlPane = () => (
    <div>
        <ShapeSelect />
        <ColorInput />
        <UpdateButton />
        <UndoRedoButtons />
    </div>
)

export default ControlPane
