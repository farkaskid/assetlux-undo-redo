import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { undoAction, redoAction } from '../actions';
import { connect } from 'react-redux'

const UndoRedoButtons = ({ canUndo, canRedo, undo, redo }) => (
    <Row style={{marginTop: '20px'}}>
        <Col xs={3} sm={3} md={3} lg={3}>
            <Button disabled={!canUndo}  bsStyle='primary' onClick={undo}>
                Undo
            </Button>
        </Col>
        <Col xs={3} sm={3} md={3} lg={3}>
            <Button disabled={!canRedo} bsStyle='primary' onClick={redo}>
                Redo
            </Button>
        </Col>
    </Row>
)

const mapStateToProps = state => ({
    canUndo: state[state.currentProject].pastStates.length > 0,
    canRedo: state[state.currentProject].futureStates.length > 0,
})

const mapDispatchToProps = dispatch => ({
    undo: () => dispatch(undoAction()),
    redo: () => dispatch(redoAction()),
})

export default connect(mapStateToProps, mapDispatchToProps)(UndoRedoButtons)
