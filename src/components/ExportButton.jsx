import React from 'react';
import { Button, Glyphicon, Row } from 'react-bootstrap';
import { exportAction } from '../actions';
import { connect } from 'react-redux';

const ExportButton = ({ download }) => (
    <Row>
        <Button bsStyle='primary' onClick={download}>
            <Glyphicon glyph='save' /> Export
        </Button>
        <a id='export' style={{ display:'none' }}></a>
    </Row>
)

const mapDispatchToProps = dispatch => ({ download: () => dispatch(exportAction()) })

export default connect(state => state, mapDispatchToProps)(ExportButton)
