import React from 'react';
import { Row } from 'react-bootstrap';
import { changeShapeAction } from '../actions';
import { connect } from 'react-redux';

const ShapeSelect = ({ isCircle, changeShape }) => (
    <Row style={{marginTop: '20px'}}>
        <select value={isCircle ? 'circle' : 'square'} name="Shapes" id="shape" onChange={event => changeShape(event.target.value)}>
            <option value="circle">Circle</option>
            <option value="square">Square</option>
        </select>
    </Row>
)

const mapStateToProps = state => ({ isCircle: state[state.currentProject].isCircle })
const mapDispatchToProps = dispatch => ({
    changeShape: shape => dispatch(changeShapeAction(shape === 'circle'))
})

export default connect(mapStateToProps, mapDispatchToProps)(ShapeSelect)
