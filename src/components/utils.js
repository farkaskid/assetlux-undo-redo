export const download = content => {
    const data = 'data:text/json;charset=utf-8,' + encodeURIComponent(content);
    const downloadAnchor = document.getElementById('export');

    downloadAnchor.setAttribute('href', data);
    downloadAnchor.setAttribute('download', 'project.json');
    downloadAnchor.click();
}

export const last = arr => arr[arr.length - 1]
