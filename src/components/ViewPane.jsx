import React from 'react';
import { Row } from 'react-bootstrap'
import Shape from './Shape';
import PreviousColors from './PreviousColors';

const ViewPane = () => (
    <div>
        <Shape />
        <PreviousColors colors={['yellow', 'green', 'lavender']} />
        <Row style={{marginTop: '10px'}}>
            Previous Colors
        </Row>
    </div>
)

export default ViewPane
