import React from 'react';
import { Row, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { updateColorAction } from '../actions';

const UpdateButton = ({ inputColor, update }) => (
    <Row style={{marginTop: '20px'}}>
        <Button disabled={inputColor === ''} bsStyle='primary' onClick={() => update(inputColor)}>
            Update Color
        </Button>
    </Row>
)

const mapStateToProps = state => ({ inputColor: state[state.currentProject].inputColor })
const mapDispatchToProps = dispatch => ({ update: color => dispatch(updateColorAction(color)) })

export default connect(mapStateToProps, mapDispatchToProps)(UpdateButton)
