import React from 'react';
import { Row } from 'react-bootstrap'
import { connect } from 'react-redux'

const Block = ({ color }) => <div style={{...blockStyle, backgroundColor: color}}></div>

const blockStyle = {
    marginLeft: '10px',
    height: '25px',
    width: '25px',
}

const PreviousColors = ({ previousColors }) => {
    const color1 = previousColors[previousColors.length - 1]
    const color2 = previousColors[previousColors.length - 2]
    const color3 = previousColors[previousColors.length - 3]

    return (
        <Row style={{marginTop: '20px'}}>
            <Block color={color1} />
            <Block color={color2} />
            <Block color={color3} />
        </Row>
    )
}

const mapStateToProps = state => ({ previousColors: state[state.currentProject].previousColors })

export default connect(mapStateToProps)(PreviousColors)
