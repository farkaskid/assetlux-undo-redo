import React from 'react';
import fetch from 'node-fetch'
import { Button, Glyphicon, Row } from 'react-bootstrap';
import { importAction } from '../actions';
import { connect } from 'react-redux';

const read = (e, callback) => {
    const input = e.target
    const reader = new FileReader()
    reader.onload = event => {
        input.value = ''
        callback(event.target.result)
    }
    reader.readAsText(input.files[0])
}

const ImportButton = ({ importProject }) => (
    <Row style={{marginTop: '10%'}}>
        <Button bsStyle='primary'>
            <Glyphicon glyph='open' />
            <label style={{ marginBottom: '0px', cursor: 'pointer' }} htmlFor='import'>Import</label>
            <input onChange={e => read(e, importProject)} type="file" name='import' id='import' style={{ display: 'none' }}/>
        </Button>
    </Row>
)

const mapDispatchToProps = dispatch => ({ importProject: content => dispatch(importAction(content)) })

export default connect(state => state, mapDispatchToProps)(ImportButton)
