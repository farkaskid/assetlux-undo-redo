import React from 'react';
import { connect } from 'react-redux';
import { changeProjectAction } from '../actions';
import { Link } from "react-router-dom";
import { Col } from 'react-bootstrap'

const Text = ({ text, currentProject }) => {
    if (text === currentProject) {
        return <strong>{text}</strong>
    }

    return text
}

const ProjectLink = ({ num, change, currentProject }) => (
    <Col xs={3} sm={3} md={3} lg={3}>
        <Link onClick={() => change(`project${num}`)} to={`/${num}`}>
            <Text currentProject={currentProject} text={`project${num}`} />
        </Link>
    </Col>
)
  
const mapDispatchToProps = dispatch => ({ change: project => dispatch(changeProjectAction(project)) })

export default connect(state => state, mapDispatchToProps)(ProjectLink)

