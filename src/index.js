import React from 'react';
import { render } from 'react-dom';
import App from './components/App.jsx';
import registerServiceWorker from './registerServiceWorker';
import { rootReducer } from './reducers';
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const store = createStore(rootReducer)

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
