import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Grid, Row, Col } from 'react-bootstrap'
import Project from './Project'
import ProjectLink from './ProjectLink';

const App = () => (
  <Router>
    <Grid>
      <Row>
        <Col xs={12} sm={12} md={12} lg={12} >
          <Route path="/:id" component={Project} />
          <hr />
        </Col>
      </Row>

      <Row>
        {[1, 2, 3, 4].map(num => <ProjectLink num={num} />)}
      </Row>
    </Grid>
  </Router>
)

export default App;
