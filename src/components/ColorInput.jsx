import React from 'react';
import { Row } from 'react-bootstrap';
import { inputColorAction } from '../actions';
import { connect } from 'react-redux'

const ColorInput = ({ input }) => (
    <Row style={{marginTop: '20px'}}>
        <label htmlFor="">Color Code:</label>
        <input type="text" onBlur={event => input(event.target.value)}/>
    </Row>
)

const mapDispatchToProps = dispatch => ({ input: color => dispatch(inputColorAction(color)) })

export default connect(state => state, mapDispatchToProps)(ColorInput)
