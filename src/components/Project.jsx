import React from 'react';
import { Row, Col } from 'react-bootstrap'
import ControlPane from './ControlPane';
import ViewPane from './ViewPane';
import ExportImportPane from './ExportImportPane';

const Project = ({ match }) => (
    <Row style={{marginTop: '100px'}}>
        <Col xs={5} sm={5} md={5} lg={5}>
            <ControlPane />
        </Col>
        <Col xs={5} sm={5} md={5} lg={5}>
            <ViewPane />
        </Col>
        <Col xs={2} sm={2} md={2} lg={2}>
            <ExportImportPane />
        </Col>
    </Row>
)

export default Project
