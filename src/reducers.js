import { 
    CHANGE_SHAPE, 
    INPUT_COLOR, 
    UPDATE_COLOR, 
    UNDO, 
    REDO, 
    EXPORT, 
    IMPORT,
    CHANGE_PROJECT
} from "./actions";
import { download } from "./components/utils";

const projectState = {
    isCircle: true,
    color: 'red',
    inputColor: '',
    previousColors: ['black', 'black', 'black'],
    pastStates: [],
    futureStates: []
}

const initialState = {
    currentProject: 'project1',
    project1: {...projectState},
    project2: {...projectState},
    project3: {...projectState},
    project4: {...projectState},
}

const snapshot = state => ({
    isCircle: state.isCircle,
    color: state.color,
    previousColors: state.previousColors,
})

export const rootReducer = (state = initialState, action) => {
    let newState;

    const { currentProject } = state
    const projectState = state[currentProject]

    switch (action.type) {
        case CHANGE_SHAPE:
            newState = {...projectState, isCircle: action.isCircle}
            newState.pastStates.push(snapshot(projectState))

            return {...state, [currentProject]: newState}

        case INPUT_COLOR:
            return {...state, [currentProject]: {...projectState, inputColor: action.inputColor}}

        case UPDATE_COLOR:
            if (action.color === projectState.color) {
                return state
            }

            const updatedPreviousColors = [...projectState.previousColors]
            updatedPreviousColors.push(projectState.color)
            newState = {...projectState, color: action.color, previousColors: updatedPreviousColors }
            newState.pastStates.push(snapshot(projectState))

            return {
                ...state,
                [currentProject]: {
                    ...projectState,
                    color: action.color, 
                    previousColors: updatedPreviousColors
                },
            }

        case UNDO:
            const futureStates = [...projectState.futureStates]
            const lastSnap = projectState.pastStates.pop()
            futureStates.push(snapshot(projectState))

            return {
                ...state,
                [currentProject]: {
                    ...projectState,
                    isCircle: lastSnap.isCircle,
                    color: lastSnap.color,
                    previousColors: lastSnap.previousColors,
                    pastStates: [...projectState.pastStates],
                    futureStates,
                },
            }

        case REDO:
            const pastStates = [...projectState.pastStates]
            const latestSnap = projectState.futureStates.pop()
            pastStates.push(snapshot(projectState))

            return {
                ...state,
                [currentProject]: {
                    ...projectState,
                    isCircle: latestSnap.isCircle,
                    color: latestSnap.color,
                    previousColors: latestSnap.previousColors,
                    pastStates,
                    futureStates: [...projectState.futureStates],
                }
            }

        case EXPORT:
            download(JSON.stringify(projectState))

            return state

        case IMPORT:
            console.log(state);
            return {
                ...state,
                [currentProject]: JSON.parse(action.content)
            }

        case CHANGE_PROJECT:
            return {...state, currentProject: action.project}
        
        default:
            return state
    }
}
