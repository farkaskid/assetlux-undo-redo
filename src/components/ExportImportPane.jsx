import React from 'react';
import ExportButton from './ExportButton';
import ImportButton from './ImportButton';

const ExportImportPane = () => (
    <div style={{marginTop: '80%'}}>
        <ExportButton />
        <ImportButton />
    </div>
)

export default ExportImportPane
