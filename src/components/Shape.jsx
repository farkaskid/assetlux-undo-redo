import React from 'react';
import { Row } from 'react-bootstrap'
import { connect } from 'react-redux'

const Shape = ({ isCircle, color }) => {
    let style = {...shapeStyle, backgroundColor: color}

    if (isCircle) {
        style.borderRadius = '50%'
    }

    return (
        <Row>
            <div style={style}></div>
        </Row>
    )
}

const shapeStyle = {
    marginTop: '20px',
    height: '120px',
    width: '120px',
}

const mapStateToProps = state => ({
    isCircle: state[state.currentProject].isCircle,
    color: state[state.currentProject].color,
})

export default connect(mapStateToProps)(Shape)
